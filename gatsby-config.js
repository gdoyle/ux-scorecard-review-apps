module.exports = {
  siteMetadata: {
    siteUrl: "https://www.yourdomain.tld",
    title: "Review App with Routes",
  },
  plugins: [],
  plugins: ["gatsby-plugin-react-helmet"],
};
